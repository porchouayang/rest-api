const express = require('express');
const app = express();

const cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');
const helmet = require('helmet');

// middlewares
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(logger('dev'));
app.use(helmet());
app.use((req, res, next) => {
    res.header(
        "Access-Control-Allow-Headers",
        "Authorization, Origin, Content-Type, Accept"
    );
    next();
});

// set CORS headers on response
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

// set Content-Type for all responses
app.use((req, res, next) => {
    res.contentType('application/json');
    next();
});

// set method headers
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    next();
});

// connect database
const { db } = require('./src/config/db.config')
const sql = require('mssql');

sql.connect(db)
    .then(() => {
        console.log('connected database ~~success~~');
    })
    .catch(er => {
        console.log(`connection database ~~failed~~ ${er}`);
    })



//  routes
require('./src/routes/category.routes')(app);



// export app
module.exports = app;
