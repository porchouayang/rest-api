const app = require('./app');

const PORT = process.env.PORT || 2022;
const HOST = "localhost" || "127.0.0.1"

app.use('', (req, res, next) => {
    res.json({
        message: `Welcome to Shop Restful API`,
        Request: {
            'method': 'GET',
            'url': `http://localhost:2022`
        },
        Response: {
            powerby: {
                language: 'node-expressjs',
                server: 'MS SQL SERVER',
            },
            develop_by: 'Mr Porchouayang',
            tel: '2078920060',
            email: 'porchouayangdevelop@gmail.com'
        }
    })
});

// start server or running on http
app.listen(PORT, () => console.log(`Server is running on http://${HOST}:${PORT}`));


