"use strict";

var express = require('express');

var app = express();

var cors = require('cors');

var bodyParser = require('body-parser');

var logger = require('morgan');

var helmet = require('helmet'); // middlewares


app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(logger('dev'));
app.use(helmet());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Headers", "Authorization, Origin, Content-Type, Accept");
  next();
}); // set CORS headers on response

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
}); // set Content-Type for all responses

app.use(function (req, res, next) {
  res.contentType('application/json');
  next();
}); // set method headers

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  next();
}); // connect database

var _require = require('./src/config/db.config'),
    db = _require.db;

var sql = require('mssql');

sql.connect(db).then(function () {
  console.log('connected database ~~success~~');
})["catch"](function (er) {
  console.log("connection database ~~failed~~ ".concat(er));
}); //  routes

require('./src/routes/category.routes')(app); // export app


module.exports = app;