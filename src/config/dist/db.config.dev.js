"use strict";

require('dotenv').config();

module.exports = {
  db: {
    user: process.env.USER,
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
    port: 1433,
    server: process.env.SERVER || "my-db.cmkqizaovr0s.us-west-2.rds.amazonaws.com",
    encrypt: false
  }
};