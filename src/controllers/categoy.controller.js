const { db } = require('../config/db.config');
const sql = require('mssql');

exports.createCategory = async (req, res) => {
    try {
        const { category } = req.body;
        category ? category : null;
        const pool = await sql.connect(db);
        await pool.request()
            .input('category', sql.NVarChar(45), category)
            .execute('create_category')
            .then(result => {
                res.status(201).json({
                    error: false,
                    status: 'OK',
                    message: "CREATE ~CATEGORY~ SUCCESS",
                    data: result.recordset
                })
            })
            .catch(e => {
                res.status(400).json({
                    error: e.message,
                    status: 'error',
                    message: 'CREATE ~CATEGORY~ FAIL'
                })
            })
    } catch (error) {
        console.log(error);
    }
}

exports.getCategories = async (req, res) => {
    try {
        const limit = req.query.limit ? parseInt(req.query.limit) : 10;
        const pageNumber = req.query.page ? parseInt(req.query.page) : 0;
        const pool = await sql.connect(db);
        await pool.request()
            .query(`SELECT *FROM getCategories c ORDER BY  c.id ASC OFFSET ${pageNumber} ROWS FETCH NEXT ${limit} ROWS ONLY`)
            .then(result => {
                res.status(200).json(result.recordset)
            })
            .catch(e => {
                res.status(404).json(e.message)
            })
    } catch (error) {
        console.log(error);
    }
}

exports.getCategory = async (req, res) => {
    try {
        const { id } = req.params;
        id ? id : null;
        const pool = await sql.connect(db);
        await pool.request()
            .input('id', sql.Int, id)
            .execute('getCategory')
            .then(result => {
                res.status(200).json(result.recordset);
            })
            .catch(e => {
                res.status(404).json(e.message)
            })
    } catch (error) {
        console.log(error);
    }
}

exports.updateCategory = async (req, res) => {
    try {

    } catch (error) {
        console.log(error);
    }
}

exports.deleteCategory = async (req, res) => {
    try {
        const { id } = req.params;
        id ? id : null;
        const pool = await sql.connect(db);
        await pool.request()
            .input('id', sql.Int, id)
            .execute('delete_category')
            .then(result => {
                res.status(200).json({
                    error: false,
                    status: 'OK',
                    message: 'DELETE ~CATEGORY~ SUCCESS',
                    data: result.recordset
                })
            })
            .catch(e => {
                res.status(400).json(e.message);
            })
    } catch (error) {
        console.log(error);
    }
}


