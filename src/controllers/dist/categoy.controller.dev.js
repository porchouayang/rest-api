"use strict";

var _require = require('../config/db.config'),
    db = _require.db;

var sql = require('mssql');

exports.createCategory = function _callee(req, res) {
  var category, pool;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          category = req.body.category;
          category ? category : null;
          _context.next = 5;
          return regeneratorRuntime.awrap(sql.connect(db));

        case 5:
          pool = _context.sent;
          _context.next = 8;
          return regeneratorRuntime.awrap(pool.request().input('category', sql.NVarChar(45), category).execute('create_category').then(function (result) {
            res.status(201).json({
              error: false,
              status: 'OK',
              message: "CREATE ~CATEGORY~ SUCCESS",
              data: result.recordset
            });
          })["catch"](function (e) {
            res.status(400).json({
              error: e.message,
              status: 'fail',
              message: 'CREATE ~CATEGORY~ FAIL'
            });
          }));

        case 8:
          _context.next = 13;
          break;

        case 10:
          _context.prev = 10;
          _context.t0 = _context["catch"](0);
          console.log(_context.t0);

        case 13:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[0, 10]]);
};

exports.getCategories = function _callee2(req, res) {
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          try {} catch (error) {
            console.log(error);
          }

        case 1:
        case "end":
          return _context2.stop();
      }
    }
  });
};

exports.getCategory = function _callee3(req, res) {
  return regeneratorRuntime.async(function _callee3$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          try {} catch (error) {
            console.log(error);
          }

        case 1:
        case "end":
          return _context3.stop();
      }
    }
  });
};

exports.updateCategory = function _callee4(req, res) {
  return regeneratorRuntime.async(function _callee4$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          try {} catch (error) {
            console.log(error);
          }

        case 1:
        case "end":
          return _context4.stop();
      }
    }
  });
};

exports.deleteCategory = function _callee5(req, res) {
  return regeneratorRuntime.async(function _callee5$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          try {} catch (error) {
            console.log(error);
          }

        case 1:
        case "end":
          return _context5.stop();
      }
    }
  });
};