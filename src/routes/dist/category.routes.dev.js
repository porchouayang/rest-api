"use strict";

var router = require('express').Router();

var controller = require('../controllers/categoy.controller');

var route = function route(app) {
  router.route('').post(controller.createCategory);
  return app.use('/category', router);
};

module.exports = route;