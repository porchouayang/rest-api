const router = require('express').Router();
const controller = require('../controllers/categoy.controller');

const route = (app) => {
    router.route('').post(controller.createCategory).get(controller.getCategories);
    router.route('/:id').get(controller.getCategory).put(controller.updateCategory).delete(controller.deleteCategory)
    return app.use('/category', router);
}

module.exports = route;

