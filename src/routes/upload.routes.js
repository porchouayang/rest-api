const router = require("express").Router();

const multer = require("multer");

const AWS = require("aws-sdk");
const { v4: uuidv4 } = require("uuid");

const s3 = new AWS.S3({
  accessKeyId: process.env.AWS_ID,
  secretAccessKey: process.env.AWS_SECRET,
});
const storage = multer.memoryStorage({
  destination: (req, file, callback) => {
    callback(null, "");
  },
});

const upload = multer({ storage }).array("files");

router.post("/upload", upload, async (req, res) => {
  if (req.files.length < 2) {
    let myFile = req.files[0].originalname.split(".");
    const fileType = myFile[myFile.length - 1];
    const date = new Date();
    const currentDate = `${date.getFullYear()}-${
      date.getMonth() + 1
    }-${date.getDate()}`;
    const params = {
      Bucket: "soulin-first-test",
      ACL: "public-read",
      Key: `ishop-${currentDate}-${uuidv4()}.${fileType}`,
      ContentType: "file/*; image/*",
      Body: req.files[0].buffer,
    };
    s3.upload(params, (error, data) => {
      if (error) {
        res.status(500).send(error);
      } else {
        res.status(200).json({
          image: [data.Location],
        });
      }
    });
  } else {
    let uploaded = [];
    for (let i = 0; i < req.files.length; i++) {
      let myFile = req.files[i].originalname.split(".");
      const fileType = myFile[myFile.length - 1];
      const date = new Date();
      const currentDate = `${date.getFullYear()}-${
        date.getMonth() + 1
      }-${date.getDate()}`;
      const params = {
        Bucket: "soulin-first-test",
        ACL: "public-read",
        Key: `ishop-${currentDate}-${uuidv4()}.${fileType}`,
        ContentType: "file/*; image/*",
        Body: req.files[i].buffer,
      };
      s3.upload(params, (error, data) => {
        console.log("dataaaaaaaaaaaaaaaaaaa:", data);
        if (error) {
          res.status(500).send(error);
        } else {
          uploaded.push(data.Location);
        }
      });
      uploaded.push(
        "https://soulin-first-test.s3-ap-southeast-1.amazonaws.com" + params.Key
      );
    }
    res.status(200).json({ image: uploaded });
  }
});

/** uploading image */
var category = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "uploads/categories");
  },
  filename: function (req, file, cb) {
    cb(null, new Date().toISOString().replace(/:/g, "-") + file.originalname);
  },
});
var uploadPhotos = multer({ storage: category });

router.post("/category-photos", uploadPhotos.single("file"), (req, res) => {
  console.log("req", req.files);
  const file = req.files["file"][0]?.path;
  res.json({ image: file });
});

var product = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "uploads/products");
  },
  filename: function (req, file, cb) {
    cb(null, new Date().toISOString().replace(/:/g, "-") + file.originalname);
  },
});
var uploadProducts = multer({ storage: product });
router.post("/product-photos", uploadProducts.array("file"), (req, res) => {
  const file = req.files["file"][0]?.path;
  res.json({ image: file });
});

module.exports = router;
